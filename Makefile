#@IgnoreInspection BashAddShebang

box-init: box-up
	cp .env.example .env
	cd laradock && \
	docker-compose exec --user=laradock workspace php artisan migrate && \
	docker-compose exec --user=laradock workspace npm install


box-up:
	cd laradock && \
	cp env-example .env && \
	docker-compose up -d nginx postgres redis workspace

go-workspace:
	cd laradock && docker-compose exec --user=laradock workspace zsh
