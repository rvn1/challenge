import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import Moment from 'moment'

export const store = new Vuex.Store({
    state: {
        loginInfo: {},
        isLogin: false,
        balance: 0,
        messages: [], // title, description, icon, color, persist, timeout
        showSnackbar: false,
    },
    mutations: {
        login (state, obj) {
            state.loginInfo = obj;
            state.isLogin = true;
        },
        setBalance(state, balance) {
            state.balance = balance;
        },
        setShowSnackbar: (state, payload) => {
            state.showSnackbar = payload
        },
        addMessage: (state, payload) => {
            if (!payload.createdAt) {
                payload.createdAt = Moment()
            }
            state.messages.unshift(payload)
            state.showSnackbar = true
        },
    },
    getters: {
        snackMessage: (state) => (
            state.showSnackbar &&
            state.messages[0] &&
            Moment().diff(state.messages[0].createdAt, 'milliseconds') < (state.messages[0].timeout || 5000)
        ) ? state.messages[0] : null,
    },
    actions: {
        addErrorMessage: (context, msg) => {
            if (!msg.color) {
                msg.color = 'error'
            }
            if (!msg.icon) {
                msg.icon = 'mdi-alert'
            }
            context.commit('addMessage', msg)
        },
        addMessage: (context, msg) => {
            context.commit('addMessage', msg)
        },
        addSuccessMessage: (context, msg) => {
            if (!msg.color) {
                msg.color = 'success'
            }
            if (!msg.icon) {
                msg.icon = 'mdi-check'
            }
            context.commit('addMessage', msg)
        },
        removeMessage: (context, index) => {
            context.commit('removeMessage', index)
        },
        setShowSnackbar: (context, val) => {
            context.commit('setShowSnackbar', val)
        },

    }
})
