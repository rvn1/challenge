import axios from 'axios'

export default {
    async list () {
        let response = await axios.get(`/api/coupons`)
        return response.data
    },

    async redeem (account_id, code) {
        let config = {
            headers: {
                "X-User-ID": account_id,
            }
        }
        let response = await axios.post(`/api/coupons/${code}/redeem`,null, config)
        return response.data
    },

    async create (obj) {
        let response = await axios.post(`/api/coupons`, obj)
        return response.data
    },

    async listConsumers (code) {
        let response = await axios.get(`/api/coupons/${code}/consumers`)
        return response.data
    },
}
