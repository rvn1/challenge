import axios from 'axios'

export default {

    async register (obj) {
        let response = await axios.post(`/api/accounts/register`, obj)
        return response.data
    },

    async balance (obj) {
        let response = await axios.post(`/api/accounts/balance`, obj)
        return response.data
    },

    async listTransactions (account_id) {
        let config = {
            headers: {
                "X-User-ID": account_id,
            }
        }
        let response = await axios.get(`/api/accounts/transactions`, config)
        return response.data
    },
}
