import Vue from 'vue'
import VueRouter from 'vue-router'
import Vuetify from "vuetify";

Vue.use(Vuetify);
import 'vuetify/dist/vuetify.min.css'
import {store} from './store';

Vue.use(VueRouter)


import App from './components/App'
import ListCoupons from './components/coupon/List'
import NewCoupon from './components/coupon/New'
import RedeemCoupon from './components/coupon/Redeem'
import ListConsumers from './components/coupon/ListConsumers'
import ListTransactions from './components/wallet/ListTransactions'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/coupons'
        },
        {
            path: '/coupons',
            name: 'couponList',
            component: ListCoupons,
        },
        {
            path: '/coupons/new',
            name: 'newCoupon',
            component: NewCoupon,
        },
        {
            path: '/coupons/redeem',
            name: 'redeemCoupon',
            component: RedeemCoupon,
        },
        {
            path: '/coupons/:code/consumers',
            name: 'listConsumers',
            component: ListConsumers,
        },
        {
            path: '/coupons/transactions',
            name: 'ListTransactions',
            component: ListTransactions,
        }
    ],
});


new Vue({
    el: '#app',
    components: {App},
    router,
    vuetify: new Vuetify(),
    store: store
})
