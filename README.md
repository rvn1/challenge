# challenge

## Development

### Instruction

* Install Docker
* Install Docker Compose
* Run `make box-init`
* Open `http://localhost`
* Run `make go-workspace` to go to workspace to work with artisan cli

Done!
