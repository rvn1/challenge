<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/accounts/register', 'AccountController@register')->name('accounts.register');
Route::post('/accounts/balance', 'AccountController@balance')->name('accounts.balance');
Route::get('/accounts/transactions', 'AccountController@transactions')->name('accounts.transactions');
Route::post('/coupons', 'CouponController@create')->name('coupons.create');
Route::get('/coupons', 'CouponController@list')->name('coupons.list');
Route::post('/coupons/{code}/redeem', 'CouponController@redeem')->name('coupons.redeem');
Route::get('/coupons/{code}/consumers', 'CouponController@listConsumers')->name('coupons.listConsumers');
