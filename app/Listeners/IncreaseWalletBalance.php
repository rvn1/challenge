<?php

namespace App\Listeners;

use App\Services\AccountService;
class IncreaseWalletBalance
{
    private $account;
    /**
     * Create the event listener.
     * @param AccountService $accountService
     * @return void
     */
    public function __construct(AccountService $accountService)
    {
        $this->account = $accountService;
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $userId = $event->userId;
        $coupon = $event->coupon;
        $this->account->IncreaseBalance($userId, $coupon['amount'], $coupon['title']);
    }
}
