<?php

namespace App\Events;

use App\Models\Coupon;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class CouponRedeemed
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $coupon;
    public $userId;

    /**
     * Create a new event instance.
     * @param Coupon $coupon
     * @param int $userId
     * @return void
     */
    public function __construct(Coupon $coupon, int $userId)
    {
        $this->coupon = $coupon;
        $this->userId = $userId;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('coupon-redeemed');
    }
}
