<?php


namespace App\Repository;

use App\Models\Wallet;

interface WalletRepositoryInterface
{
    public function create(int $userId): Wallet;
    public function find(int $id): ?Wallet;
    public function increaseBalance(int $userId, int $amount, string $description);
    public function ListTransactions(int $userId): iterable;

}
