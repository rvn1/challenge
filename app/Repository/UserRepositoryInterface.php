<?php

namespace App\Repository;

use App\Models\User;

interface UserRepositoryInterface
{
    public function findByPhone(int $countryCode, int $phoneNumber): ?User;
    public function create(int $countryCode, int $phoneNumber): User;
}
