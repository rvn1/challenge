<?php


namespace App\Repository;


use App\Models\Coupon;

interface CouponRepositoryInterface
{
    public function create(string $title, string $code, int $redeem_limit, int $durationLimit, int $amount): Coupon;

    public function list(): iterable;

    public function increaseUsedCounter(string $code, int $userId): Coupon;

    public function findByCode(string $code): ?Coupon;

    public function ListConsumers(string $code): iterable;
}
