<?php


namespace App\Repository\Eloquent;


use App\Exceptions\CouponAlreadyExistsException;
use App\Exceptions\CouponAlreadyRedeemedException;
use App\Exceptions\CouponNotFoundException;
use App\Models\Consumer;
use App\Models\Coupon;
use App\Repository\CouponRepositoryInterface;
use Illuminate\Support\Facades\DB;
use App\Exceptions\CouponLimitReachedException;

class CouponRepository implements CouponRepositoryInterface
{
    public function create(string $title, string $code, int $redeem_limit, $durationLimit, int $amount): Coupon
    {
        if (Coupon::where('code', '=', $code)->exists() ) {
            throw new CouponAlreadyExistsException("coupon with same code '{$code}' already exists");
        }
        return Coupon::firstOrCreate([
            'title' => $title,
            'code' => $code,
            'redeem_limit' => $redeem_limit,
            'amount' => $amount,
            'duration_limit' => $durationLimit
        ]);
    }

    public function list(): iterable
    {
        return Coupon::all();
    }

    public function findByCode(string $code): ?Coupon {
        return Coupon::where('code', '=', $code)->lockForUpdate()->first();
    }

    public function increaseUsedCounter(string $code, int $userId): Coupon
    {
        DB::beginTransaction();
        try {
            $coupon = Coupon::where('code', '=', $code)->lockForUpdate()->first();
            if (Consumer::where('coupon_id', '=', $coupon['id'])->where('user_id', '=', $userId)->exists() ) {
                throw new CouponAlreadyRedeemedException("coupon '{$code}' already redeemed");
            }
            $coupon['used_count'] = $coupon['used_count'] +1;
            $coupon->save();
            Consumer::create([
                'user_id' => $userId,
                'coupon_id' =>$coupon['id']
            ]);
            DB::commit();
            return $coupon;
        } catch (\Exception $e) {
            DB::rollback();
            throw($e);
        }

    }

    public function ListConsumers(string $code): iterable
    {
        $coupon = Coupon::where('code', '=', $code)->lockForUpdate()->first();
        if ($coupon == null) {
            throw new CouponNotFoundException("Coupon with code '{$code}' not found");
        }

        return Consumer::where('coupon_id', '=', $coupon['id'])->get();

    }
}
