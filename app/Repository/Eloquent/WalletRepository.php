<?php


namespace App\Repository\Eloquent;


use App\Models\Transaction;
use App\Models\Wallet;
use App\Repository\WalletRepositoryInterface;
use Illuminate\Support\Facades\DB;

class WalletRepository implements WalletRepositoryInterface
{
    public function create(int $user_id): Wallet
    {
        return Wallet::firstOrCreate([
            'user_id' => $user_id
        ]);

    }

    public function find(int $id): ?Wallet
    {
        return Wallet::find($id);
    }

    public function increaseBalance(int $userId, int $amount, string $description)
    {
        DB::beginTransaction();
        try {
            $wallet = Wallet::where('user_id', '=', $userId)->lockForUpdate()->first();
            $wallet['balance'] = $wallet['balance'] + $amount;
            $wallet->save();

            Transaction::create([
                'wallet_id' => $wallet['id'],
                'amount' => $amount,
                'type' => 'deposit',
                'description' => $description
            ]);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            throw($e);
        }
    }

    public function ListTransactions(int $userId): iterable
    {
        $wallet = Wallet::where('user_id', '=', $userId)->first();
        return $transactions = Transaction::where('wallet_id', '=', $wallet['id'])->get();
    }
}
