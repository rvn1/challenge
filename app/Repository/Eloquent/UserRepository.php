<?php

namespace App\Repository\Eloquent;

use App\Models\User;
use App\Repository\UserRepositoryInterface;


class UserRepository implements UserRepositoryInterface
{

    public function findByPhone($countryCode, $phoneNumber): ?User
    {
        return User::where('country_code', $countryCode)->where('phone_number', $phoneNumber)->first();
    }

    public function create(int $countryCode, int $phoneNumber): User
    {
        return  User::firstOrCreate([
            'country_code' => $countryCode,
            'phone_number' => $phoneNumber
        ]);

    }
}
