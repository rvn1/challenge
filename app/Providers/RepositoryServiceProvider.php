<?php

namespace App\Providers;

use App\Repository\UserRepositoryInterface;
use App\Repository\WalletRepositoryInterface;
use App\Repository\CouponRepositoryInterface;
use App\Repository\Eloquent\UserRepository;
use App\Repository\Eloquent\WalletRepository;
use App\Repository\Eloquent\CouponRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(UserRepositoryInterface::class, UserRepository::class);
        $this->app->bind(WalletRepositoryInterface::class, WalletRepository::class);
        $this->app->bind(CouponRepositoryInterface::class, CouponRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
