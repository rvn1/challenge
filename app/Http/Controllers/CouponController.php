<?php

namespace App\Http\Controllers;

use App\Services\CouponService;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    protected $couponService;

    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    public function create(Request $request)
    {
        $coupon = $this->couponService->Create($request->input('title'), $request->input('code'), $request->input('redeem_limit'), $request->input('duration_limit'), $request->input('amount'));
        return response()
            ->json($coupon);
    }

    public function list()
    {
        $coupons = $this->couponService->list();
        return response()
            ->json($coupons);
    }

    public function redeem(Request $request, string $code)
    {
        $userID = (int) $request->header('X-User-ID');
        $this->couponService->Redeem($code, $userID);
    }

    public function listConsumers(Request $request, string $code)
    {
        $consumers = $this->couponService->ListConsumers($code);
        return response()
            ->json($consumers);
    }
}
