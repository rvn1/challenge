<?php

namespace App\Http\Controllers;

use App\Services\AccountService;
use Illuminate\Http\Request;

class AccountController extends Controller
{

    protected $accountService;

    public function __construct(AccountService $accountService)
    {
        $this->accountService = $accountService;
    }

    public function register(Request $request)
    {
        $rsp = $this->accountService->Register($request->input('country_code'), $request->input('phone_number'));
        return response()
            ->json($rsp);
    }

    public function balance(Request $request)
    {
        $rsp = $this->accountService->Balance($request->input('country_code'), $request->input('phone_number'));
        return response()
            ->json($rsp);
    }

    public function transactions(Request $request)
    {
        $userID = (int) $request->header('X-User-ID');
        $rsp = $this->accountService->Transactions($userID);
        return response()
            ->json($rsp);
    }
}
