<?php


namespace App\Services;

use App\Repository\UserRepositoryInterface;
use App\Repository\WalletRepositoryInterface;

class AccountService
{
    private $userRepository;
    private $walletRepository;

    public function __construct(UserRepositoryInterface $userRepository, WalletRepositoryInterface $walletRepository)
    {
        $this->userRepository = $userRepository;
        $this->walletRepository = $walletRepository;
    }

    public function Register(string $countryCode, string $phoneNumber): array
    {
        $user = $this->userRepository->create($countryCode, $phoneNumber);
        $wallet = $this->walletRepository->create($user['id']);
        $wallet = $this->walletRepository->find($wallet['id']);
        return ['id'=>$user['id'], 'balance'=>$wallet['balance']];
    }

    public function Balance(string $countryCode, string $phoneNumber): array
    {
        $user = $this->userRepository->create($countryCode, $phoneNumber);
        $wallet = $this->walletRepository->create($user['id']);
        $wallet = $this->walletRepository->find($wallet['id']);
        return ['balance'=>$wallet['balance']];
    }

    public function IncreaseBalance(int $userId, int $amount, string $description) {
        $this->walletRepository->increaseBalance($userId, $amount, $description);
    }

    public function Transactions(int $userId): iterable {
        return $this->walletRepository->ListTransactions($userId);
    }
}
