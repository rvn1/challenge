<?php


namespace App\Services;


use App\Events\CouponRedeemed;
use App\Exceptions\CouponHasBeenExpiredException;
use App\Exceptions\CouponLimitReachedException;
use App\Exceptions\CouponNotFoundException;
use App\Models\Coupon;
use App\Repository\CouponRepositoryInterface;

class CouponService
{
    private $couponRepository;

    public function __construct(CouponRepositoryInterface $couponRepository)
    {
        $this->couponRepository = $couponRepository;
    }

    public function Create(string $title, string $code, int $redeemLimit, $durationLimit, int $amount): Coupon
    {
        return $this->couponRepository->create($title, $code, $redeemLimit, $durationLimit, $amount);
    }

    public function List(): iterable
    {
        return $this->couponRepository->List();
    }

    public function Redeem(string $code, int $userId)
    {
        $coupon = $this->couponRepository->findByCode($code);
        if ($coupon == null) {
            throw new CouponNotFoundException("Coupon with code '{$code}' not found");
        }
        if ($coupon['is_expired']) {
            throw new CouponHasBeenExpiredException("Coupon with code '{$code}' has been expired");
        }
        if ($coupon['used_count'] == $coupon['redeem_limit']) {
            throw new CouponLimitReachedException('coupon limit reached');
        }

        $coupon = $this->couponRepository->increaseUsedCounter($code, $userId);

        event(new CouponRedeemed($coupon, $userId));
    }

    public function ListConsumers(string $code): iterable {
        return $this->couponRepository->ListConsumers($code);
    }
}
