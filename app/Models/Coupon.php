<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'code', 'amount', 'used_count', 'redeem_limit', 'duration_limit'];

    public function getIsExpiredAttribute()
    {
        $expiresTime = (new \DateTime($this['created_at']))->add(new \DateInterval('PT' . $this['duration_limit'] . 'M'));

        if( new \DateTime() > $expiresTime ) {
            return true;
        }
        return false;
    }

    protected $appends = ['is_expired'];

    protected $attributes = [
        'used_count' => 0,
    ];

}
