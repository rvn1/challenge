<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Consumer extends Model
{
    protected $fillable = ['user_id', 'coupon_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    protected $with = ['user'];
}
